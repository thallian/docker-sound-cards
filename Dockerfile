FROM registry.gitlab.com/thallian/docker-alpine-s6:master

ENV VERSION 6bae986b

RUN addgroup -g 2222 carder
RUN adduser -h /var/lib/carder -u 2222 -D -G carder carder

RUN apk --no-cache add libressl tar

RUN wget -qO- https://gitlab.com/thallian/sound-cards/repository/archive.tar.gz?ref=$VERSION | tar -xz -C /var/lib/carder --strip 1
RUN rm -r /var/lib/carder/src
RUN find /var/lib/carder/ -maxdepth 1 -type f -print0 | xargs -0 rm --

ADD /rootfs /

RUN chown -R carder:carder /var/lib/carder

EXPOSE 8000

VOLUME /var/lib/carder/static/sound
