Run a [Sound-Card](https://gitlab.com/thallian/sound-cards) website.

# Volumes
- `/var/lib/carder/static/sound`

# Port
- 8000

# Capabilities
- CHOWN
- DAC_OVERRIDE
- NET_BIND_SERVICE
- SETGID
- SETUID
